package example.com.myapplication.Models

data class Student(
        val name: String,
        val age: Int,
        val type: Int
)
