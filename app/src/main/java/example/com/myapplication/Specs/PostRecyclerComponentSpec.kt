package example.com.myapplication.Specs

import com.facebook.litho.Component
import com.facebook.litho.ComponentContext
import com.facebook.litho.annotations.*
import com.facebook.litho.sections.SectionContext
import com.facebook.litho.sections.common.DataDiffSection
import com.facebook.litho.sections.common.RenderEvent
import com.facebook.litho.sections.widget.RecyclerCollectionComponent
import com.facebook.litho.widget.ComponentRenderInfo
import com.facebook.litho.widget.RenderInfo
import example.com.myapplication.Components.FeedVideoComponent
import example.com.myapplication.Components.LeftListItemComponent
import example.com.myapplication.Models.Student

@LayoutSpec
object PostRecyclerComponentSpec {

    @OnCreateLayout
    fun onCreateLayout(c: ComponentContext, @Prop dataModels: List<Student>): Component {
        return RecyclerCollectionComponent.create(c)
                .section(DataDiffSection.create<Student>(SectionContext(c))
                        .data(dataModels)
                        .renderEventHandler(PostRecyclerComponent.onRender(c))
                        .build())
                .disablePTR(true)
                .build()
    }

    @OnEvent(RenderEvent::class)
    fun onRender(c: ComponentContext, @FromEvent model: Student): RenderInfo =
            ComponentRenderInfo.create()
                    .component(
                            when (model.type) {
                                0 ->
                                    LeftListItemComponent.create(c)
                                            .model(model)
                                            .build()
                                else -> FeedVideoComponent.create(c)
                                        .build()
                            }
                    )
                    .build()
}
