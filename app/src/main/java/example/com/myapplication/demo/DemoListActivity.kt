package example.com.myapplication.demo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.facebook.litho.ComponentContext
import com.facebook.litho.LithoView
import example.com.myapplication.Models.Student
import example.com.myapplication.Specs.PostRecyclerComponent

class DemoListActivity : AppCompatActivity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val componentContext = ComponentContext(this)
        setContentView(LithoView.create(this,
                PostRecyclerComponent.create(componentContext).dataModels(getDataModels())
                        .build()))
    }

    fun getDataModels(): List<Student> {
        val arr = ArrayList<Student>();
        for (i in 1..1000) {
            arr.add(Student("Student " + i, i + 18, i % 2))
        }
        return arr
    }
}
