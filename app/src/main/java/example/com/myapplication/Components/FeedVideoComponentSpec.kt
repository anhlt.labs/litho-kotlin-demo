package example.com.myapplication.Components

import android.graphics.Color
import com.facebook.litho.Column
import com.facebook.litho.Component
import com.facebook.litho.ComponentContext
import com.facebook.litho.Row
import com.facebook.litho.annotations.LayoutSpec
import com.facebook.litho.annotations.OnCreateLayout
import com.facebook.litho.widget.Text

@LayoutSpec
object FeedVideoComponentSpec {

    @OnCreateLayout
    fun onCreateLayout(c: ComponentContext): Component {
        return Column.create(c)
                .child(Text.create(c)
                        .text("A")
                        .backgroundColor(Color.RED)
                        .widthPercent(100f)
                        .aspectRatio(16 / 9f)
                        .build())
                .build()
    }
}
