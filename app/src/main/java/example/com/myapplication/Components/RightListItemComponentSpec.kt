package example.com.myapplication.Components

import android.graphics.Color.BLACK
import android.graphics.Color.GRAY
import com.facebook.litho.Column
import com.facebook.litho.Component
import com.facebook.litho.ComponentContext
import com.facebook.litho.annotations.LayoutSpec
import com.facebook.litho.annotations.OnCreateLayout
import com.facebook.litho.annotations.Prop
import com.facebook.litho.widget.Text
import com.facebook.yoga.YogaAlign
import com.facebook.yoga.YogaEdge.ALL
import example.com.myapplication.Models.Student

@LayoutSpec
object RightListItemComponentSpec {

    @OnCreateLayout
    fun onCreateLayout(c: ComponentContext, @Prop model: Student): Component {
        return Column.create(c)
                .paddingDip(ALL, 16f)
                .alignItems(YogaAlign.FLEX_END)
                .child(Text.create(c).text("Name is ${model.name}").textSizeSp(18f).textColor(BLACK).build())
                .child(Text.create(c).text("Age is ${model.age}").textSizeSp(16f).textColor(GRAY).build())
                .build()
    }
}
